const express = require('express');
const bodyParser = require('body-parser');
const handlebars = require('express-handlebars');

var app = express();
let server = require('http').Server(app);
require('./socketio')(server);

app.engine('.hbs', handlebars({
  defaultLayout: 'main',
  extname: '.hbs',
  helpers: {
    section: function(name, options) {
      if(!this._sections) {this._sections = {}}
      this._sections[name] = options.fn(this);
      return null;
    }
  }
}));
app.set('view engine', '.hbs');

const api = require('./api');

app.set('port', process.env.PORT || 3000);

app.use(bodyParser.json());
app.use(express.static('public'));
app.use('/', api);

app.use(function(req, res) {
  res.type('text/plain');
  res.status(404);
  res.send('404 Page not found');
});

app.use(function(err, req, res, next) {
  console.log(err);
  res.type('text/plain');
  res.status(500);
  res.end('500 - Internal server error');
});

server.listen(app.get('port'), function() {
  console.log(`Express запущен на http://localhost:${app.get('port')};
нажмите Ctrl+C для завершения.`);
});
