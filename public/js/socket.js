(function() {
  var socket = io();
  var sendButton = document.getElementById('send-message');
  var textInput = document.getElementById('m');
  var lists = document.getElementsByClassName('messages')[0];
  var rooms = document.getElementsByClassName('rooms')[0];

  textInput.addEventListener('keypress', function(e) {
      if (e.keyCode === 13) {
        sendMessage();
      }
  });

  sendButton.addEventListener('click', function(e) {
    sendMessage();
  })

  rooms.addEventListener('click', function(e) {
    if (e.target.tagName === 'SPAN') {
      var clicked = e.target.dataset['room'];

      makeActiveRoom(clicked);
      showActiveChat(clicked);

      socket.emit('room', getActiveItem());
    }
  })

  socket.on('chat message', function(msg){
      appendMessage(msg);
  });
  socket.on('user connect', function(msg){
      appendMessage(msg);
  });
  socket.on('user disconnect', function(msg){
      appendMessage(msg);
  });

  function makeActiveRoom(clicked) {
    var roomList = document.getElementsByClassName('room');
    roomList = [].slice.apply(roomList);
    roomList.forEach(function(room) {
        deleteClass(room, 'active');
    });
    var newActive = document.getElementsByClassName('room_' + clicked)[0];
    addClass(newActive, 'active');
  }

  function showActiveChat(clicked) {
    var messageList = document.getElementsByClassName('message-list');
    messageList = [].slice.apply(messageList);
    messageList.forEach(function(messageL) {
        addClass(messageL, 'hidden');
    });
    var newActive = document.getElementsByClassName('message_' + clicked)[0];
    deleteClass(newActive, 'hidden');
  }

  function deleteClass(e, c) {
    var regexp = new RegExp("\\b" + c + "\\b\\s*", "g");
    e.className = e.className.replace(regexp, "");
  }

  function addClass(e, c) {
    e.className += " " + c;
  }

  function sendMessage() {
    socket.emit('chat message', getActiveItem(), m.value);
    m.value = '';
  }

  function appendMessage(msg) {
    var text = document.createTextNode(msg);
    var li = document.createElement('li');
    li.appendChild(text);
    var list = document.getElementsByClassName('message_' + getActiveItem())[0];
    list.appendChild(li);
  }

  function getActiveItem() {
    return document.getElementsByClassName('active')[0].dataset['room'];
  }
})();
