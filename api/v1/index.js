const express = require('express');
let app = module.exports = express();

let rooms = require('../../rooms');

app.get('/', function(req, res) {
    res.render('index', {layout: 'socket', rooms});
});
