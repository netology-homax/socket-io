let rooms = require('../rooms');

module.exports = function(server) {

  var io = require('socket.io')(server);

  io.on('connection', function(socket){
    socket.join('room1');
    socket.broadcast.emit('user connect', 'A user connected');

    socket.on('room', function(msg) {
        rooms.forEach(room => socket.leave(room.name));
        socket.join(msg);
        socket.broadcast.to(msg).emit('user connect', 'A user connected');
    });

    socket.on('chat message', function(id, msg){
      io.to(id).emit('chat message', msg);
    });

    socket.on('disconnect', function(){
      socket.broadcast.emit('user disconnect', 'A user disconnected');
    });
  });
}
